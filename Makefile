# This Makefile is designed to be included by another Makefile located in your project directory.
# ==> https://github.com/EmakinaFR/docker-magento2/wiki/Makefile

SHELL := /bin/sh
CONSOLE = bin/console
GIT = git

amend:
	git commit --amend --no-edit
